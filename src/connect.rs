use crate::DbLoginError;

// All credentials objects must implement this trait.
// The implementation should execvp into the process providing the database session.
pub trait Connect {
  fn connect<H: Into<String>>(self, host: H, port: u16) -> DbLoginError;
}
