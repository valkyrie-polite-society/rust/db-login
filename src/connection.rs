use serde::{Deserialize, Serialize};
use std::fs::File;
use crate::DbLoginError;
use crate::vault_auth::VaultSpec;
use crate::GenerateCredentialsSpec;
use crate::vault_auth::VaultAuth;
use crate::GenerateCredentials;
use crate::connect::Connect;
use std::path::PathBuf;

#[derive(Serialize, Deserialize, Clone)]
pub struct Connection {
  pub description: String,
  pub vault: VaultSpec,
  pub credentials: GenerateCredentialsSpec,
  pub endpoint: DbHostSpec
}

#[derive(Serialize, Deserialize, Clone)]
pub struct DbHostSpec {
  pub host: String,
  pub port: u16
}

impl Connection {
  pub fn establish(self) -> std::result::Result<(), DbLoginError> {
    let client = self.vault.auth.get_client(self.vault.host)?;
    let credentials = self.credentials.generate(client)?;
    Err(credentials.connect(self.endpoint.host, self.endpoint.port))
  }
}

#[derive(Serialize, Deserialize)]
pub struct Connections {
  pub connections: Vec<Connection>
}

pub fn from_file(path: PathBuf) -> Result<Vec<Connection>, DbLoginError> {
  let file = File::open(path)?;
  let reader = std::io::BufReader::new(file);
  let config: Connections = serde_yaml::from_reader(reader)?;
  Ok(config.connections)
}
