use exec::Command;
use hashicorp_vault::{Client, client::TokenData, client::VaultResponse};
use serde::{Serialize, Deserialize};
use thiserror::Error;
use crate::DbLoginError::{ExecError, LogicError, VaultError};
use hashicorp_vault::client::VaultClient;
use crate::GenerateCredentialsSpec::Cassandra;
use crate::connect::Connect;
use terminal_menu::*;

mod connect;
mod connection;
mod tests;
mod vault_auth;

fn main() -> std::result::Result<(), DbLoginError> {
  let  file = dirs::home_dir().unwrap().join(".db-login.yaml");
  let connections = connection::from_file(file)?;

  let mut menu_items: Vec<TerminalMenuItem> = Vec::new();
  menu_items.push(label("Databases"));
  for c in &connections {
    menu_items.push(button(c.description.clone()));
  }
  let menu = menu(menu_items);
  run(&menu);

  {
    let mm = mut_menu(&menu);
    let connection = connections.iter().nth(mm.selected_item_index() - 1).unwrap().clone();
    connection.establish()
  }
}

#[derive(Serialize, Deserialize, Clone)]
pub enum GenerateCredentialsSpec {
  #[serde(rename = "cassandra")]
  Cassandra(GenerateCassandraCredentials)
}

impl GenerateCredentials for GenerateCredentialsSpec {
  type DbCredentials = CassandraCredentials;

  fn generate(self, client: VaultClient<TokenData>) -> Result<Self::DbCredentials, DbLoginError> {
    match self {
      Cassandra(c) => c.generate(client)
    }
  }
}

#[derive(Deserialize)]
pub struct CassandraCredentials {
  username: String,
  password: String
}

#[derive(Error, Debug)]
pub enum DbLoginError {
  #[error("vault error")]
  VaultError(#[from] hashicorp_vault::Error),
  #[error("vault url error")]
  VaultUrlError(#[from] hashicorp_vault::url::ParseError),
  #[error("exec error")]
  ExecError(#[from] exec::Error),
  #[error("serde json error")]
  SerdeJsonError(#[from] serde_json::Error),
  #[error("serde yaml error")]
  SerdeYamlError(#[from] serde_yaml::Error),
  #[error("io error")]
  IoError(#[from] std::io::Error),
  #[error("logic error")]
  LogicError(String)
}

fn get_cassandra_credentials<Mount: Into<String>, Role: Into<String>>(client: Client<TokenData>, db_mount: Mount, db_role: Role) -> std::result::Result<CassandraCredentials, DbLoginError>
{
  let mount = db_mount.into();
  let role = db_role.into();

  let resp: VaultResponse<CassandraCredentials> = client.get_secret_engine_creds(mount.as_str(), role.as_str())?;
  match resp.data {
    Some(credentials) => Ok(credentials),
    None => Err(LogicError("Vault API succeeded, but no Cassandra credentials were returned".to_string()))
  }
}

impl Connect for CassandraCredentials {
  fn connect<H: Into<String>>(self, host: H, port: u16) -> DbLoginError {
    ExecError(Command::new("cqlsh")
      .arg("--username")
      .arg(self.username)
      .arg("--password")
      .arg(self.password)
      .arg(host.into())
      .arg(port.to_string())
      .exec())
  }
}

// All credential generation methods must implement this trait
// The implementation should use the provided information to generate DB credentials from Vault
pub trait GenerateCredentials {
  type DbCredentials;

  fn generate(self, client: Client<TokenData>) -> std::result::Result<Self::DbCredentials, DbLoginError>;
}

#[derive(Serialize, Deserialize, Clone)]
pub struct GenerateCassandraCredentials {
  mount: String,
  role: String
}

impl GenerateCredentials for GenerateCassandraCredentials {
  type DbCredentials = CassandraCredentials;
  fn generate(self, client: Client<TokenData>) -> std::result::Result<CassandraCredentials, DbLoginError> {
    get_cassandra_credentials(client, self.mount, self.role)
  }
}
