#[cfg(test)]
mod tests {
  use crate::vault_auth::VaultAuthToken;
  use crate::GenerateCassandraCredentials;
  use crate::connection::DbHostSpec;
  use crate::connection::Connection;
  use crate::GenerateCredentialsSpec;
  use crate::vault_auth::VaultAuthSpec;
  use crate::vault_auth::VaultSpec;

  #[test]
  fn connection_serialization_works() {
    let vault_auth = VaultAuthToken {
      id: "fake".to_string()
    };

    let role = GenerateCassandraCredentials {
      mount: "dse1312acab".to_string(),
      role: "readonly".to_string(),
    };

    let database = DbHostSpec {
      host: "127.0.0.1".to_string(),
      port: 9042
    };

    let connection = Connection {
      description: "Cassandra - Local".to_string(),
      vault: VaultSpec {
        host: "http://127.0.0.1:8200".to_string(),
        auth: VaultAuthSpec::Token(vault_auth)
      },
      credentials: GenerateCredentialsSpec::Cassandra(role),
      endpoint: database
    };

    let expected = r#"---
description: Cassandra - Local
vault:
  host: "http://127.0.0.1:8200"
  auth:
    token:
      id: fake
credentials:
  cassandra:
    mount: dse1312acab
    role: readonly
endpoint:
  host: 127.0.0.1
  port: 9042
"#;

    let s = serde_yaml::to_string(&connection);
    assert_eq!(s.is_ok(), true);

    match serde_yaml::to_string(&connection) {
      Ok(actual) => {
        assert_eq!(actual, expected);
      },
      Err(e) => {
        assert!(false, "{}", e)
      }
    }
  }
}
