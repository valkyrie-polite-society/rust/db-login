use hashicorp_vault::TryInto;
use hashicorp_vault::url::Url;
use hashicorp_vault::Client;
use hashicorp_vault::client::VaultClient;
use hashicorp_vault::client::TokenData;
use crate::DbLoginError;
use serde::{Deserialize, Serialize};
use crate::vault_auth::VaultAuthSpec::Token;
use crate::VaultError;

// All Vault authentication methods must implement this trait.
// The implementation should return a Vault client with valid token, or an error
pub trait VaultAuth {
  fn get_client<U: TryInto<Url, Err = hashicorp_vault::client::error::Error>>(self, url: U) -> std::result::Result<Client<TokenData>, DbLoginError>;
}

#[derive(Serialize, Deserialize, Clone)]
pub struct VaultSpec {
  pub host: String,
  pub auth: VaultAuthSpec
}

#[derive(Serialize, Deserialize, Clone)]
pub enum VaultAuthSpec {
  #[serde(rename = "token")]
  Token(VaultAuthToken)
}

impl VaultAuth for VaultAuthSpec {
  fn get_client<U: TryInto<Url, Err=hashicorp_vault::client::error::Error>>(self, url: U) -> Result<VaultClient<TokenData>, DbLoginError> {
    match self {
      Token(t) => t.get_client(url)
    }
  }
}

#[derive(Serialize, Deserialize, Clone)]
pub struct VaultAuthToken {
  pub id: String
}

impl VaultAuth for VaultAuthToken {
  fn get_client<U: TryInto<Url, Err = hashicorp_vault::client::error::Error>>(self, url: U) -> std::result::Result<Client<TokenData>, DbLoginError> {
    Client::new(url, self.id)
      .map_err(|e| VaultError(e))
  }
}
